package inputbyte.backend;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import Server.Server;

@DisplayName("Test server")
public class ServerTest {
    static Server server = null;
    
    @BeforeClass
    @DisplayName("Turn on server")
    public static void init() throws IOException{
        server = Server.getInstance();
        server.Start(8000);
    }

    @Test
    @DisplayName("Server exists")
    public void serverInstance() throws IOException{
        assertTrue(Server.getInstance() != null);
    }

    @Test
    @DisplayName("Server creates connection")
    public void serverConnection() throws IOException{
        assertTrue(server.getConnection() != null);
    }

    @AfterClass
    public static void serverOff() throws SQLException{
        server.Close();
    }
}
