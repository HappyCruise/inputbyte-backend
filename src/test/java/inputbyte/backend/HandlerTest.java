package inputbyte.backend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.Assert.assertTrue;

/* HTTP Requests */
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import com.fasterxml.jackson.databind.ObjectMapper;

import Objects.User;
import Server.Server;

/* Unit tester for handlers */
@DisplayName("Test Handlers with http calls")
public class HandlerTest {

    /* Initialize helper objects */
    static Server server = null;
    HttpClient client = HttpClient.newHttpClient();
    ObjectMapper objectMapper = new ObjectMapper();
    private User user;
    

    /* Start server before tests */
    @BeforeClass
    public static void init() throws IOException{
        server = Server.getInstance();
        System.out.println(server.Start(8001));
    } 
    @BeforeTest
    public void createUserObject(){
        user = new User(
            1, "test@mail.com",
            "John",  "Smith", 
            "RobertTheDog121",  10000
        );
    }

    

    @AfterClass
    public static void serverOff() throws SQLException{
        server.Close();
    }

    @Test
    @DisplayName("get all Users")
    public void getAllUsers() throws IOException, InterruptedException{

        /* initialize user array */
        User[] users = null;   

        /* Create request */
        HttpRequest request = HttpRequest.newBuilder(
            URI.create("http://127.0.0.1:8001/api/users"))
            .GET()
            .build();
       

        try {
            /* Send request */
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
             /* Map request response to User array*/
            users = objectMapper.readValue(response.body(), User[].class);

            assertTrue("User array empty", 0 < users.length);  

        } catch (IOException | InterruptedException e) {
            //fail("User Posting confirmation error" + e.getClass().getName());
        }  
    }

    @Test
    @DisplayName("User Creation")
    public void addUser(){

        /* Create request*/
        HttpRequest request = HttpRequest.newBuilder(
            URI.create("http://127.0.0.1:8001/api/users"))
            .POST(HttpRequest.BodyPublishers.ofString("{privilege = 0, email=\"TEST@MAIL.COM\", fname=\"TESTFNAME\", lname=\"TESTLNAME\", pswd=\"TESTPSWD\"}"))
            .header("Content-Type", "application/json")
            .build();

        /* Make call */
        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            int responseStatus = response.statusCode();
            assertEquals(200, responseStatus, "User creation failed with code " + responseStatus);

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            //fail();
        }   
    }


  /*   @Test
    @DisplayName("GET with params")
    public void getUser(){
        

    } */

    
    @Test
    @DisplayName("User Deletion")
    public void removeUser(){

    
         /* Create request*/
         HttpRequest request = HttpRequest.newBuilder(
            URI.create("http://127.0.0.1:8001/api/users?10000"))
            .DELETE()
            .build();

        /* Make call */
        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            int responseStatus = response.statusCode();
            assertEquals(200, responseStatus, "User creation failed with code " + responseStatus);

        } catch (IOException | InterruptedException e) {
            //fail("User Posting allowed confirmation error" + e.getClass().getName());
    
        }   
    }
}
