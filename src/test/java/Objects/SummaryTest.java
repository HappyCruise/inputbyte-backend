package Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import junit.framework.Assert;

public class SummaryTest {
    Summary summary;
    private ArrayList<Feedback> feedbacks = new ArrayList<>();
    //private ArrayList<Integer> ratings = new ArrayList<>();
    private int sum;
    private double average;

    private Feedback feedback;
    private Feedback feedback2;
    //private NewFeedback newFeedback;
    private int id, rating, lessonid, courseid;
    private String comments;

    @Before
    @DisplayName("Create test objects")
    public void createFeedbackObject() {
        rating = 2;
        comments = "Great Test !";
        id = 100100;
        lessonid = 30030;
        courseid = 20;
        feedback = new Feedback(rating, comments, id, lessonid, courseid);
        //newFeedback = new NewFeedback(rating, comments, lessonid, courseid);
        feedbacks.add(feedback);

        rating = 4;
        comments = "Great Test !";
        id = 100101;
        lessonid = 30030;
        courseid = 20;
        feedback2 = new Feedback(rating, comments, id, lessonid, courseid);
        //newFeedback = new NewFeedback(rating, comments, lessonid, courseid);
        feedbacks.add(feedback2);
        summary = new Summary();
        
        for(Feedback f : feedbacks){
            summary.addFeedback(f);
        }
        

    }

    @Test
    @DisplayName("Confirm feedbacks length")
    public void confirmFeedbacksLenght() {
        assertEquals(2, feedbacks.size());
        assertNotEquals(0, feedbacks.size(), "The result cannot be 0");
        assertTrue(summary.ratingsCount()==true);
    }


    @Test
    @DisplayName("Confirm add feedback")
    public void confirmAddFeedback() {

        assertEquals(feedbacks.size(), summary.feedbacks.size());
    }
    
    @Test
    @DisplayName("Confirm sum")
    public void SumTest(){
        sum = summary.getSum();
       
        assertEquals(6, sum);
    }
    
    @Test
    @DisplayName("Confirm average")
    public void AverageTest(){
        
        assertEquals(3.0, summary.Average(), 0.00);

    }
    

}
