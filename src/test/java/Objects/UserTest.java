package Objects;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

@DisplayName("User object test")
public class UserTest {

    private User user;
    private NewUser newUser;


    @Before
    @DisplayName("Create user objects")
    public void createUserObject(){
        user = new User(
            1, "test@mail.com",
            "John",  "Smith", 
            "RobertTheDog121",  0
        );

     
        newUser = new NewUser(
        0, "bean@bbc.com",
        "Rowan",  "Atkinson", 
        "MrBeanTeddyBear"       
        );
    }


    @Test
    @DisplayName("Confirm user values")
    public void confirmUserValues(){
        assertEquals(1, user.getPrivilege());
        assertEquals("test@mail.com", user.getEmail());
        assertEquals("John", user.getFname());
        assertEquals("Smith", user.getLname());
        assertEquals("RobertTheDog121", user.getPswd());
        assertEquals(0, user.getId());
    }


    @Test
    @DisplayName("Confirm newUser values")
    public void confrimNewUserValues(){
        assertEquals("bean@bbc.com", newUser.getEmail());
        assertEquals("Rowan", newUser.getFname());
        assertEquals("Atkinson", newUser.getLname());
        assertEquals("MrBeanTeddyBear", newUser.getPswd());
    }
}
