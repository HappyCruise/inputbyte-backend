 package Objects;

import static org.junit.Assert.assertEquals;

import java.sql.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

@DisplayName("Lesson object test")
public class LessonTest {
    private Date date, newdate;
    private String room, lessonPassword, description;
    private int id, courseID;
    private Lesson lesson;
    
    private String newroom, newlessonPassword, newdescription;
    private Lesson newLesson;
    private int newcourseID;
    
    @Before
    @DisplayName("Create test objects")
    public void createLessonObject(){
        date = Date.valueOf("2022-10-15");
        room = "testroom"; lessonPassword = "123456"; description = "Test case lorem ipsum";
        id = 104030; courseID = 40200;
        lesson = new Lesson(id, date, room, lessonPassword, description, courseID);
        
        newdate = Date.valueOf("2022-10-12");
        newroom = "NewTestroom6A35"; newlessonPassword = "654331"; newdescription = "A new lesson";
        newcourseID = 40300;
        newLesson = new Lesson(id, newdate, newroom, newlessonPassword, newdescription, newcourseID);
  
    }

    
    @Test
    @DisplayName("Confirm lesson values")
    public void confirmLessonValues(){
        assertEquals(id, lesson.getId());
        assertEquals(date, lesson.getDate());
        assertEquals(room, lesson.getRoom());
        assertEquals(lessonPassword, lesson.getLessonpassword());
        assertEquals(description, lesson.getDescription());
        assertEquals(courseID, lesson.getCourseId());
    }
    
    @Test
    @DisplayName("Confirm newLesson values")
    public void confirmNewLessonValues(){
        assertEquals(newdate, newLesson.getDate());
        assertEquals(newroom, newLesson.getRoom());
        assertEquals(newlessonPassword, newLesson.getLessonpassword());
        assertEquals(newdescription, newLesson.getDescription());
        assertEquals(newcourseID, newLesson.getCourseId());
    }
}
 