package Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

/* Unit tester for Objects */
@DisplayName("Course object test")
public class CourseTest {

    private int id, user_id;
    private String name;
    Course course;
    NewCourse newCourse;


    @Before
    @DisplayName("Create test objects")
    public void createCourseObjects(){
        id = 1010; user_id = 2003;
        name ="Test CourseX-B0X1"; 

        course = new Course(id, name, user_id);
        newCourse = new NewCourse(name, user_id);
    }

    @Test
    @DisplayName("Confirm course values")
    public void confirmCourseValues(){
        assertEquals(id, course.getId());
        assertEquals(name, course.getName());
        assertEquals(user_id, course.getUser_id());
        
    }        
    @Test
    @DisplayName("Confirm newCourse values")
    public void confirmNewCourseValues(){

        assertEquals(name, newCourse.getName());
        assertEquals(user_id, newCourse.getTeacher_id());
    }
    
}
