package Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

@DisplayName("Feedback object test")
public class FeedbackTest {

    private Feedback feedback;
    private NewFeedback newFeedback;
    private int id, rating, lessonid, courseid;
    private String comments;

    
    @Before
    @DisplayName("Create the test objects")
    public void createFeedbackObjects(){
        rating = 5;
        comments = "Great Test !";
        id = 100100;
        lessonid = 30030;
        courseid = 20;
        feedback = new Feedback(rating, comments, id, lessonid, courseid);
        newFeedback = new NewFeedback(rating, comments, lessonid, courseid);
    }

    @Test
    @DisplayName("Confirm feedback values")
    public void confirmFeedbackValues(){
        assertEquals(rating, feedback.getRating());
        assertEquals(comments, feedback.getComments());
        assertEquals(id, feedback.getId());
        assertEquals(lessonid, feedback.getLessonid());
        assertEquals(courseid, feedback.getCourseid());
    }

    @Test
    @DisplayName("Confirm newFeedback values")
    public void confirmNewFeedbackValues(){
        assertEquals(rating, newFeedback.getRating());
        assertEquals(comments, newFeedback.getComments());
        assertEquals(lessonid, newFeedback.getLessonid());
        assertEquals(courseid, newFeedback.getCourseid());
    }

    
}
