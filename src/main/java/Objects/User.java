package Objects;

/**
 * Class User object is used to map database data into a JSON object.
 * @author Heini
 */
public class User {
    private int privilege;
    private String email;
    private String fname;
    private String lname;
    private String pswd;
    private int id;

    public User(){
        
    }
    /**
     * Constructs and initializes a user
     * 
     * @param _privilegethe user's privilege indicates whether the user is an admin or a teacher
     * @param _email email of the user login
     * @param _firstname firstname of the user
     * @param _lastname lastname of the user
     * @param _password password for user login
     * @param _id id for user identification
     */
    public User(int _privilege, String _email, String _firstname, String _lastname, String _password, int _id){
        this.privilege = _privilege;
        this.email = _email;
        this.fname = _firstname;
        this.lname = _lastname;
        this.pswd = _password;
        this.id = _id;
    }
    /**
     * get user's priviledge
     * 
     * @return the priviledge return 1 if user is admin, 0 if user is teacher
     */
    
    public int getPrivilege() {
        return privilege;
    }
    /**
     * get user's email for login
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * get user's firsname
     * 
     * @return the firstname
     */

    public String getFname() {
        return fname;
    }
    /**
     * get user's lastname
     * 
     * @return the lastname
     */

    public String getLname() {
        return lname;
    }
    /**
     * get user's password for login
     * 
     * @return password
     */

    public String getPswd() {
        return pswd;
    }
    /**
     * get user's id
     * 
     * @return the id
     */

    public int getId() {
        return id;
    }
}
