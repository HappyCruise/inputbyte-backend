package Objects;

//Object is used to insert JSON data into database
/**
 * Class EditUser object is used to edit user by insert JSON data into database
 */
public class EditUser {
    private int privilege;
    private String email;
    private String fname;
    private String lname;
    private String pswd;
    private int id;
    /**
     * get user's priviledge
     * 
     * @return the priviledge return 1 if user is admin, 0 if user is teacher
     */
    public int getPrivilege() {
        return privilege;
    }
    /**
     * privilege setter method
     * 
     * @param privilege privilege 1 if user is admin or 0 if user is teacher
     */

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }
    /**
     * get user's email for login
     * 
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * email setter method 
     * 
     * @param email email of user to login
     */

    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * get user's firsname
     * 
     * @return the firstname
     */

    public String getFname() {
        return fname;
    }
    /**
     * firstname setter method
     * 
     * @param fname firstname of the user
     */

    public void setFname(String fname) {
        this.fname = fname;
    }
    /**
     * get user's lastname
     * 
     * @return the lastname
     */

    public String getLname() {
        return lname;
    }
    /**
     * lastname setter method
     * 
     * @param lname lastname of the user
     */

    public void setLname(String lname) {
        this.lname = lname;
    }
    /**
     * get user's password for login
     * 
     * @return password
     */

    public String getPswd() {
        return pswd;
    }
    /**
     * password setter method
     * 
     * @param pswd password for the user to login
     */

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }
    /**
     * get user's id
     * 
     * @return the id
     */

    public int getId() {
        return id;
    }
    /**
     * id setter method
     * 
     * @param id id for the user
     */

    public void setId(int id) {
        this.id = id;
    }
}