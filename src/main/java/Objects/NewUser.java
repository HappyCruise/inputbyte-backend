package Objects;



//Object is used to insert JSON data into database
/**
 * Class NewUser object is used to insert JSON data into database 
 */
public class NewUser {
    private int privilege;
    private String email;
    private String fname;
    private String lname;
    private String pswd;

    public NewUser(){};
    /**
     * Constructs and initializes a new user
     * 
     * @param _privilege new user's privilege indicates whether the user is an admin or a teacher
     * @param _email email of the new user login
     * @param _fname firstname of the new user
     * @param _lname lastname of the new user
     * @param _pswd password for new user login
     */

    public NewUser(int _privilege, String _email, String _fname, String _lname, String _pswd){
        this.privilege = _privilege;
        this.email = _email;
        this.fname = _fname;
        this.lname = _lname;
        this.pswd = _pswd;
    }
    /**
     * get new user's priviledge
     * 
     * @return the privilege return 1 if new user is admin, 0 if user is teacher
     */

    public int getPrivilege() {
        return privilege;
    }
    /**
     * privilege setter method
     * 
     * @param privilege privilege 1 if new user is admin or 0 if new user is teacher
     */

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }
    /**
     * get new user's email for login
     * 
     * @return the email
     */

    public String getEmail() {
        return email;
    }
    /**
     * email setter method 
     * 
     * @param email email of the new user to login
     */

    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * get new user's firsname
     * 
     * @return the firstname
     */

    public String getFname() {
        return fname;
    }
    /**
     * firstname setter method
     * 
     * @param fname firstname of the new user
     */

    public void setFname(String fname) {
        this.fname = fname;
    }
    /**
     * get new user's lastname
     * 
     * @return the lastname
     */
    public String getLname() {
        return lname;
    }
    /**
     * lastname setter method
     * 
     * @param lname lastname of the new user
     */

    public void setLname(String lname) {
        this.lname = lname;
    }
    /**
     * get new user's password for login
     * 
     * @return password
     */

    public String getPswd() {
        return pswd;
    }
    /**
     * password setter method
     * 
     * @param pswd password for the new user to login
     */

    public void setPswd(String pswd) {
        this.pswd = pswd;
    }
}
