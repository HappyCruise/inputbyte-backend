package Objects;

/**
 * Class Feedback bbject is used to map database data into a JSON object.
 * 
 * @author Heini
 */
public class Feedback {
    private int rating;
    private String comments;
    private int id;
    private int lessonid;
    private int courseid;
    
/**
 * Constructs and initializes a Feedback
 * 
 * @param _rating rating from one to ten for lesson
 * @param _comments feedback comment on the lesson
 * @param _id id for feedback identification
 * @param _lessonid lessonid for feedback
 * @param _courseid courseid for feedback
 */

    public Feedback(int _rating, String _comments, int _id, int _lessonid, int _courseid){
        this.rating = _rating;
        this.comments = _comments;
        this.id = _id;
        this.lessonid = _lessonid;
        this.courseid = _courseid;
    }
    /**
     * get feedback's rating
     * 
     * @return rating from one to ten
     */

    public int getRating(){
        return rating;
    }
    /**
     * get feedback's comment
     * 
     * @return the written comment
     */

    public String getComments(){
        return comments;
    }
    /**
     * get feedback's id
     * 
     * @return id for feedback identification
     */

    public int getId(){
        return id;
    }
    /**
     * get lesson's id for feedback
     * feedback reference to the lesson
     * 
     * @return the lessons id 
     */

    public int getLessonid(){
        return lessonid;
    }
    /**
     * get course's id for new feedback
     * feedback reference to the course
     * 
     * @return the course id
     */
    
    public int getCourseid(){
        return courseid;
    }
}
