package Objects;


/**
 * Class course object is used to map database data into a JSON object.
 * 
 * @author inputByte-team
 */
public class Course {
    private int id;
    private String name;
    private int user_id;

    /**
     * Constructs and initializes a Course
     * 
     * @param _id id for course identification
     * @param _name name for course
     * @param _user_id id of the teacher or admin's
     */

    public Course(int _id, String _name, int _user_id) {
        this.id = _id;
        this.name = _name;
        this.user_id = _user_id;
    }
    /**
     * get course's id
     * 
     * @return the id
     */

    public int getId() {
        return id;
    }
    /**
     * get course's name
     * 
     * @return the name
     */

    public String getName() {
        return name;
    }
    /**
     * get course's userId
     * course reference to its user
     * 
     * @return user_id
     */

    public int getUser_id() {
        return user_id;
    }

}
