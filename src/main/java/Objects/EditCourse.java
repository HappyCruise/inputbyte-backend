package Objects;

/**
 * Class EditCourse object is used to edit course by insert JSON data into database
 */
public class EditCourse {
    private String name;
    private int user_id;
    private int id;
    /**
     * get edit course's id
     * 
     * @return the id
     */

    public int getId() {
        return id;
    }
    /**
     * id setter method
     * 
     * @param id the id of the editCourse
     */

    public void setId(int id) {
        this.id = id;
    }
    /**
     * get edit course userId
     * course reference to its user
     * 
     * @return user_id
     */

    public int getUser_id() {
        return user_id;
    }
    /**
     * userID setter method
     * course reference to its teacher
     * 
     * @param user_id the userID for the editCourse
     */
    
    public void setTeacher_id(int user_id) {
        this.user_id = user_id;
    }
    /**
     * get edit course name
     * 
     * @return name
     */
    
    public String getName() {
        return name;
    }
    /**
     * name setter method
     * 
     * @param name the name for the editCourse
     */
    
    public void setName(String name) {
        this.name = name;
    }
}
