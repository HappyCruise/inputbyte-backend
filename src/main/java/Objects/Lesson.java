package Objects;

import java.sql.Date;


/**
 * Class Lesson object is used to map database data into a JSON object.
 * 
 * @author Heini
 */
public class Lesson {
    private int id;
    private Date date;
    private String room;
    private String lessonpassword;
    private String description;
    private int courseid;
    /**
     * Constructs and initializes a Lesson
     * 
     * @param _id id for lesson identification
     * @param date datetime for lesson
     * @param _room classroom for lesson
     * @param _lessonPassword password to log in to lesson
     * @param _description a written description of the lesson
     * @param _courseID id for the course to which the lesson belongs
     */

    public Lesson(int _id, Date date, String _room, String _lessonPassword, String _description, int _courseID) {
        this.id = _id;
        this.date = date;
        this.room = _room;
        this.lessonpassword = _lessonPassword;
        this.description = _description;
        this.courseid = _courseID;
    }
    /**
     * get lesson's date
     * 
     * @return the datetime
     */

    public Date getDate() {
        return date;
    }
    /**
     * get lesson's classroom
     * 
     * @return the classroom
     */

    public String getRoom() {
        return room;
    }
    /**
     * get lesson's generated password
     * 
     * @return the password
     */
    public String getLessonpassword() {
        return lessonpassword;
    }
    /**
     * get lesson's id
     * 
     * @return the id
     */

    public int getId() {
        return id;
    }
    /**
     * get the written description of the lesson
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * get lesson's course_id
     * lesson reference to its course
     * 
     * @return courseid
     */

    public int getCourseId() {
        return courseid;
    }
}
