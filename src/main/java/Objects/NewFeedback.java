package Objects;

/**
 * Class NewFeedback object is used to insert JSON data into database
 * 
 * @author Heini
 */
public class NewFeedback {
    private int rating;
    private String comments;
    private int lessonid;
    private int courseid;

    public NewFeedback(){}
    /**
     * Constructs and initializes a NewFeedback
     * 
     * @param _rating rating from one to ten for lesson
     * @param _comments feedback comment on the lesson
     * @param _lessonid lessonid for feedback
     * @param _courseID courseid for feedback
     */
    public NewFeedback(int _rating, String _comments, int _lessonid, int _courseID){
        this.rating = _rating;
        this.comments = _comments;
        this.lessonid= _lessonid;
        this.courseid = _courseID;
    }
    /**
     * get new feedback's rating
     * 
     * @return rating from one to ten
     */

    public int getRating(){
        return rating;
    }
    /**
     * rating from one to ten setter method
     * 
     * @param rating rating from one to ten of the new feedback
     */

    public void setRating(int rating){
        this.rating = rating;
    }
    /**
     * get new feedback's comment
     * 
     * @return the written comment
     */

    public String getComments(){
        return comments;
    }
    /**
     * comment setter method
     * 
     * @param comments comments of the new feedback
     */

    public void setComments(String comments){
        this.comments = comments;
    }
    /**
     * get lesson's id for new feedback
     * feedback reference to the lesson
     * 
     * @return the lessons id 
     */

    public int getLessonid(){
        return lessonid;
    }
    /**
     * lesson id setter method
     * feedback reference to the lesson
     * 
     * @param lessonid lessonid of the new feedback
     */

    public void setLessonid(int lessonid){
        this.lessonid = lessonid;
    }
    /**
     * get course's id for new feedback
     * feedback reference to the course
     * 
     * @return the course id
     */

    public int getCourseid(){
        return courseid;
    }
    /**
     * course id setter method
     * feedback reference to the course
     * 
     * @param courseid courseid of the new feedback
     */

    public void setCourseid(int courseid){
        this.courseid = courseid;
    }
}
