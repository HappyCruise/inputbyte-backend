package Objects;

/**
 * Class NewLesson object is used to insert JSON data into database
 * @author Heini
 */
public class NewLesson {
    private String time;
    private String room;
    private String lessonpassword;
    private String description;
    private int courseid;

    public NewLesson(){}
    /**
     * Constructs and initializes a NewLesson
     * 
     * @param _time datetime for new lesson
     * @param _room classroom for new lesson
     * @param _lessonpassword password to log in to new lesson
     * @param _description a written description of the new lesson
     * @param _courseid id for the course to which the new lesson belongs
     */

    public NewLesson(String _time, String _room, String _lessonpassword, String _description, int _courseid){
        this.time = _time;
        this.room = _room;
        this.lessonpassword = _lessonpassword;
        this.description = _description;
        this.courseid = _courseid;
    }
    /**
     * get new lesson's date
     * 
     * @return the datetime
     */

    public String getTime() {
        return time;
    }
    /**
     * time setter method
     * 
     * @param time time of the new lesson
     */
    public void setTime(String time){
        this.time=time;
    }
    /**
     * get new lesson's classroom
     * 
     * @return the classroom
     */

    public String getRoom() {
        return room;
    }
    /**
     * room setter method
     * 
     * @param room room of the new lesson
     */
    public void setRoom(String room){
        this.room=room;
    }
    /**
     * get new lesson's generated password
     * 
     * @return the password
     */

    public String getLessonpassword() {
        return lessonpassword;
    }
    /**
     * lessonpassword setter method
     * 
     * @param lessonpassword password of the new lesson
     */
    public void setLessonpassword(String lessonpassword){
        this.lessonpassword=lessonpassword;
    }
    /**
     *  get the written description of the new lesson
     * @return the description
     */

    public String getDescription() {
        return description;
    }
    /**
     * description setter method
     * @param description description of the new lesson
     */
    public void setDescription(String description){
        this.description=description;
    }
    /**
     * get new lesson's course_id
     * lesson reference to its course
     * 
     * @return courseid
     */

    public int getCourseid() {
        return courseid;
    }
    /**
     * courseid setter method
     * lesson reference to its course
     * 
     * @param courseid courseid of the new lesson
     */
    public void setCourseid(int courseid){
        this.courseid=courseid;
    }
    
}
