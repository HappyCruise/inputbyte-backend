package Objects;

/**
 * Class NewCourse object is used to insert JSON data into database
 * 
 * @author Heini
 */
public class NewCourse {
    private String name;
    private int teacher_id;

    public NewCourse(){};
    /**
     * Constructs and initializes a NewCourse
     * 
     * @param _name name for new course
     * @param _teacher_id id of the teacher or admin
     */
    
    public NewCourse(String _name, int _teacher_id) {
        this.name = _name;
        this.teacher_id = _teacher_id;
    }
    /**
     * get new course's teacher_id
     * course reference to its teacher
     * 
     * @return the teacher_id
     */

    public int getTeacher_id() {
        return teacher_id;
    }
    /**
     * teacher_id setter method
     * course reference to its teacher
     * 
     * @param teacher_id teacher id of the new course
     */
    
    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }
    /**
     * get new course's name
     * 
     * @return the name
     */
    
    public String getName() {
        return name;
    }
    /**
     * name setter method
     * 
     * @param name name of the new course
     */
    
    public void setName(String name) {
        this.name = name;
    }
}
