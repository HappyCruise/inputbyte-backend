package Objects;

import java.sql.Date;

/**
 * Class EditLesson object is used to edit lesson by insert JSON data into database 
 */
public class EditLesson {
    private Date date;
    private String room;
    private String lessonpassword;
    private String description;
    private int courseId;
    private int id;
    /**
     * get editlesson's id
     * 
     * @return the id
     */

    public int getId() {
        return id;
    }
    /**
     * id setter method
     * 
     * @param id the id of the editLesson
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * get editlesson's date time
     * 
     * @return the date 
     */
    public Date getDate() {
        return date;
    }
    /**
     * date setter method
     * 
     * @param date the date of the editLesson
     */
    public void setDate(Date date){
        this.date=date;
    }
    /**
     * get editlesson's class room
     * 
     * @return the room
     */

    public String getRoom() {
        return room;
    }
    /**
     * class room setter method
     * 
     * @param room the room of the editLesson
     */
    public void setRoom(String room){
        this.room=room;
    }
    /**
     * get editLesson's password
     * 
     * @return the editlesson's password
     */

    public String getLessonpassword() {
        return lessonpassword;
    }
    /**
     * lesson's password setter method
     * 
     * @param lessonpassword the lessonspassword of the editLesson
     */
    public void setLessonPassword(String lessonpassword){
        this.lessonpassword=lessonpassword;
    }
    /**
     * get editLesson's description
     * 
     * @return the editLesson's description
     */

    public String getDescription() {
        return description;
    }
    /**
     * description setter method
     * 
     * @param description the description of the editLesson
     */
    public void setDescription(String description){
        this.description=description;
    }
    /**
     * get editLesson's course Id
     * lesson reference to its course
     * 
     * @return the courseId
     */

    public int getCourseId() {
        return courseId;
    }
    /**
     * courseid setter method
     * lesson reference to its course
     * 
     * @param courseid the courseid of the editLesson
     */
    public void setCourseId(int courseid){
        this.courseId=courseid;
    }
    
}
