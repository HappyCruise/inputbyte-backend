package Objects;

import java.util.ArrayList;
/**
 * Class Summary calculates the average of the lesson's feedback rating
 * 
 * @author Heini
 */

public class Summary {

    ArrayList<Feedback> feedbacks = new ArrayList<>();

    private int sum;
    private float average = 0f;
    /**
     * method adds feedback to the feedback list
     * 
     * @param feedback feedback object
     */

    public void addFeedback(Feedback feedback) {
        feedbacks.add(feedback);
    }
    /**
     * the method calculates the average of the items in the feedback list
     * <p>
     * method calls the getSum method to calculate the sum of the rating
     * method calls the ratingsCount method to check if the numerator is greater than zero
     * if ratingsCount is TRUE then average is the sum divided by the size of the list
     * 
     * @return the average of feedbacks ratings
     */

    public float Average() {

        
        sum = getSum();
        System.out.println("summa " + sum);
        System.out.println("määrä " + feedbacks.size());
        if (ratingsCount() == true) {
            average = sum / feedbacks.size();
            System.out.println("Average" + average);
            System.out.printf("Double upto 1 decimal places: %.1f", average);
        }
        return average;
    }
    /**
     * test if there are feedbacks in the list
     * 
     * @return true if feedbacs list size is bigger than zero
     */
    public boolean ratingsCount(){
        if(feedbacks.size() > 0){
            return true;
        } else {
        return false;
        }
    }
    /**
     * get summary's average 
     * 
     * @return the average
     */
    public float getAverage() {
        return average;
    }
    /**
     * get the sum of the ratings of the feedback in the feedbacks list
     * 
     * @return sum
     */
    
    public Integer getSum() {
        for (Feedback feedback : feedbacks) {
           
            sum += feedback.getRating();
            
        }
        
        return sum;
    }
}
