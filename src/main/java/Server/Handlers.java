package Server;

import java.sql.Connection;
import com.sun.net.httpserver.HttpHandler;

import Handlers.CourseHandler;
import Handlers.FeedbackHandler;
import Handlers.LessonHandler;
import Handlers.UserHandler;

public class Handlers {
    /**
     * The SQL connection to be used by the handlers
     */
    protected Connection connection = null;

    /**
     * The HttpHandlers for the server
     */
    public HttpHandler courseHandler, lessonHandler, userHandler, feedbackHandler;


    /**
     * Creates the handlers with given database Connection
     * 
     * @param _connection Connection to the database
     */
    // Connection is required for database queries
    public Handlers(Connection _connection) {
        this.connection = _connection;
        courseHandler = new CourseHandler(connection).getHandler();
        lessonHandler = new LessonHandler(connection).getHandler();
        userHandler = new UserHandler(connection).getHandler();
        feedbackHandler = new FeedbackHandler(connection).getHandler();
    }
}
