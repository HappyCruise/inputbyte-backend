package Server;

import java.io.IOException;
import java.sql.SQLException;

public class Main {
    /**
     * Turns on the server
     * @param args
     * @throws SQLException
     * @throws IOException
     */
    public static void main( String[] args ) throws SQLException, IOException{

       //Create the server
        Server server = Server.getInstance();
       
        //Start the server
        System.out.println(server.Start(8080));
    }
}
