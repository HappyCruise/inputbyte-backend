package Server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpServer;

public class Server {
    
	/**
	 * The server Singleton instance
	 */
	private static Server instance = new Server();
	/**
	 * the java server. com.sun.net.HttpServer 
	 */
	private HttpServer server;
	/**
	 * The Connector for connection to database
	 */
	private DatabaseConnector connector;
	/**
	 * The handlers for the server
	 */
	private Handlers handlers;
	
	/**
	 * Creates the server object
	 */
	private Server(){};

	/**
	 * Returns the server instance
	 * 
	 * @return 				the server instance
	 * @throws IOException	
	 */
	public static Server getInstance() throws IOException {	
		return instance;
	}
	
	/**
	 * Starts the server with given port number and creates the handlers
	 * 
	 * @param port	specify the port number to run at
	 * @return 		the server address
	 */
	public String Start(int port) throws IOException  {
	    
	    //Create the Connection to database
        connector = new DatabaseConnector();
        connector.CreateConnection();
        
        
        //Create the handlers
        handlers = new Handlers(connector.getConnection());
        
		try {
		    //Create the service
			server = HttpServer.create(new InetSocketAddress(port), 0);
			
			//Create the context for routes and handlers
			server.createContext("/api/courses", handlers.courseHandler);
			server.createContext("/api/lessons", handlers.lessonHandler);
			server.createContext("/api/users", handlers.userHandler);
			server.createContext("/api/feedbacks", handlers.feedbackHandler);
			
			//Create a default executor for the Thread
			server.setExecutor(null);
			
			//Start the service
			server.start();
			System.out.println("Connection established.");
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return server.getAddress().toString();
	}


	/**
	 * Returns the Connection from the Database connector
	 * 
	 * @return	the Connection
	 */
	public Connection getConnection(){
		return connector.getConnection();
	}

	/**
	 * Stops the server
	 * 
	 * @throws SQLException
	 */
	public void Close() throws SQLException{
		connector.CloseConnection();
		server.stop(0);
	}

}
