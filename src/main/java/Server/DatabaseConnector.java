package Server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
  

public class DatabaseConnector {
    /**
     * The database address
     */
    private String url = "jdbc:mariadb://10.114.34.3:3306/InputByte";
    /**
     * Database username     
     * */
    private String user = "admin";
    /**
     * Database password
     */
    private String pass = "1234";

    /**
     * Database connection
     */
    private Connection connection;
    
    /**
     * Create the connection to the database
     * 
     */
    public void CreateConnection(){
        try {
            //Use the mariadb driver
            Class.forName("org.mariadb.jdbc.Driver");
            
            //Create the connection with address, username and password
            connection =  DriverManager.getConnection(url, user, pass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Returns the sql Connection to the database
     * 
     * @return  sql Connection
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Closes the Connection to database
     * 
     * @throws SQLException
     */
    public void CloseConnection() throws SQLException{
        this.connection.close();
    }

}
