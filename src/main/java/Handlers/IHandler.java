package Handlers;

import com.sun.net.httpserver.HttpHandler;

public interface IHandler {
    /**
     * Return the handler
     * @return  HttpHandler
     */
    public HttpHandler getHandler();

}
