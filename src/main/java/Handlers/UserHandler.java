package Handlers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

import Objects.NewUser;
import Objects.User;
import Objects.EditUser;

public class UserHandler extends Handler implements IHandler {

    /**
     * Create the handler with given Connection
     * @param _connection   the Connection
     */
    public UserHandler(Connection _connection) {
        super(_connection);
    }

    public HttpHandler userHandler = new HttpHandler() {

        /**
         * Handle calls to user 
         * 
         * @param exchange  the HttpExchange
         */
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            // Get the request method
            String method = exchange.getRequestMethod();

            // Add headers to response.
            addResponseHeaders(exchange);

            // Add the opening '[' to response for mapping to JSON
            String response = "";

            // Create the JSON mapper
            ObjectMapper obj = new ObjectMapper();

            // Compare request type
            if (method.equals("GET")) {

                String reqURI = exchange.getRequestURI().getQuery();
                String pass = "", user = "", type = "";
                User userObj = null;

                try {
                    // Map params
                    Map<String, String> parms = queryToMap(reqURI);

                    // define ids
                    pass = parms.get("pass");
                    user = parms.get("user");
                    type = parms.get("type");
                } catch (Exception e) {
                    // Incorrect request
                    exchange.sendResponseHeaders(400, response.getBytes().length);
                }

                /* If user and password defined with type login */
                if (!user.equals("") && !pass.equals("") && type.equals("login")) {
                    /* Search for user */
                    userObj = getUser(user, pass);

                    /* No user found, Incorrect credentials */
                    if (userObj == null) {
                        exchange.sendResponseHeaders(401, response.getBytes().length);
                    }
                    /* User found */
                    else {
                        response += obj.writeValueAsString(userObj);
                        exchange.sendResponseHeaders(200, response.getBytes().length);
                        sendResponseBody(response, exchange);
                    }

                }
                /* User and password defined with type all */
                else if (!user.equals("") && !pass.equals("") && type.equals("all")) {
                    userObj = getUser(user, pass);

                    /* No user found, Incorrect credentials */
                    if (userObj == null) {
                        exchange.sendResponseHeaders(401, response.getBytes().length);
                    }
                    /* User found */
                    else {
                        /* User has neccessary privilege */
                        if (userObj.getPrivilege() == 1) {
                            String sql = "SELECT * FROM User";
                            response = "[";

                            // Call the database
                            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                                // Map the result to a resultset
                                ResultSet resultSet = statement.executeQuery();
                                while (resultSet.next()) {

                                    // Map results to a User object
                                    userObj = new User(resultSet.getInt(1), resultSet.getString(2),
                                            resultSet.getString(3),
                                            resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6));

                                    // Map the user object to JSON and add to response
                                    try {
                                        response += obj.writeValueAsString(userObj);
                                        response += ",";

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // Remove ',' after last object and add enclosing ']'
                            response = response.substring(0, response.length() - 1) + "]";

                            // Send response headers
                            exchange.sendResponseHeaders(200, response.getBytes().length);

                            // Send response body and close exchange
                            sendResponseBody(response, exchange);
                        }
                        /* User doesnt have privilege */
                        else {
                            exchange.sendResponseHeaders(403, response.getBytes().length);
                        }
                    }
                } else {
                    // Incorrect request
                    exchange.sendResponseHeaders(400, response.getBytes().length);
                }

            } else if (method.equals("POST")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);

                /* TODO --------- */
                // If no body, check for url params
                if (reqBody == null || reqBody == "") {
                    reqBody = exchange.getRequestURI().getQuery();
                    System.out.println("reqbody" + reqBody);
                }

                /* --------------- */
                // Map the JSON to an object
                NewUser newuser = obj.readValue(reqBody, NewUser.class);

                // Create SQL Query with Object
                String sql = "INSERT INTO User(User_privilege, User_email, User_fname, User_lname, User_pswd) VALUES ('"
                        + newuser.getPrivilege() + "','" + newuser.getEmail() + "','" + newuser.getFname() + "','"
                        + newuser.getLname() + "','" + newuser.getPswd() + "');";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send the response body and close exchange
                sendResponseBody(response, exchange);

            } else if (method.equals("OPTIONS")) {
                exchange.getResponseHeaders().add("Allowed", "GET, OPTIONS, POST, DELETE, PUT");
                exchange.sendResponseHeaders(200, exchange.getResponseHeaders().size());

            } else if (method.equals("PUT")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);
                // Map the string to an object
                EditUser edituser = obj.readValue(reqBody, EditUser.class);

                String sql = "Update User SET  User_privilege = " + edituser.getPrivilege() +
                        ", User_email = '" + edituser.getEmail() +
                        "', User_fname = '" + edituser.getFname() +
                        "', User_lname = '" + edituser.getLname() +
                        "', User_pswd = '" + edituser.getPswd() +
                        "' WHERE User_id = " + edituser.getId() +
                        ";";

                System.out.println(sql);
                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }

            else if (method.equals("DELETE")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);

                // If no body, check for url params
                if (reqBody == null || reqBody == "") {
                    reqBody = exchange.getRequestURI().getQuery();
                }

                String sql = "DELETE FROM User WHERE User_id = "+ reqBody + ";";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }
        }

    };

    /**
     * Return single user from database with password and email address
     * @param user      The email address
     * @param pass      The password
     * @return          null if not found, User object if found
     */
    private User getUser(String user, String pass) {
        String sql = "SELECT * FROM User WHERE User_email = '" + user + "' AND User_pswd = '" + pass + "';";

        // Call the database
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            // Map the result to a resultset
            ResultSet resultSet = statement.executeQuery();

            /* If user was found */
            if (resultSet.next()) {
                // Map found user to User object
                User userObj = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
                        resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6));

                // Send found user
                return userObj;

            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public HttpHandler getHandler() {
        return userHandler;
    }

}
