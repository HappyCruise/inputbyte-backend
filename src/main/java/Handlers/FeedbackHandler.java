package Handlers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

import Objects.Feedback;
import Objects.NewFeedback;
import Objects.Summary;

public class FeedbackHandler extends Handler implements IHandler {
    /**
     * Create the handler with given Connection
     * @param _connection   the Connection
     */
    public FeedbackHandler(Connection _connection) {
        super(_connection);
    }

    // Handler for /api/feedback
    public HttpHandler feedbackHandler = new HttpHandler() {

        /**
         * Handle calls to feedback
         */
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            // Get the request method
            String method = exchange.getRequestMethod();
            System.out.println(method);

            // Add headers to response.
            addResponseHeaders(exchange);

            // Add the opening '[' to response for mapping to JSON
            String response = "[";

            // Create the JSON mapper
            ObjectMapper obj = new ObjectMapper();

            // Compare request type
            if (method.equals("GET")) {
                // Return all feedbacks
                String sql = "SELECT * FROM Feedback";

                String reqURI = exchange.getRequestURI().getQuery();

                if (reqURI != null) {
                    // Map params
                    Map<String, String> parms = queryToMap(reqURI);

                    // define ids
                    String course = parms.get("course");
                    String lesson = parms.get("lesson");

                    // Check if course or lesson is defined
                    if (course != null && !course.equals("")) {
                        sql += " WHERE Course_id = " + course;
                    } else if (lesson != null && !lesson.equals("")) {
                        sql += " WHERE Lesson_id = " + lesson;
                    }
                }

                sql += ";";

                // summary for calculating the average
                Summary summary = new Summary();
                // Call the database
                try (PreparedStatement statement = connection.prepareStatement(sql)) {
                    // Map the result to a resultset
                    ResultSet resultSet = statement.executeQuery();

                    while (resultSet.next()) {

                        // Map results to a feedback object
                        Feedback feedback = new Feedback(resultSet.getInt(1), resultSet.getString(2),
                                resultSet.getInt(3),
                                resultSet.getInt(4), resultSet.getInt(5));
                        // added to the feedback for summary list
                        summary.addFeedback(feedback);

                        // Map the feedback object to JSON and add to response
                        try {
                            response += obj.writeValueAsString(feedback);
                            response += ",";

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (response.length() > 1) {
                    summary.Average();
                    response += summary.getAverage();

                    // Remove ',' after last object and add enclosing ']'
                    response = response.substring(0, response.length()) + "]";
                    // Send response headers
                    exchange.sendResponseHeaders(200, response.getBytes().length);

                    // Send response body and close exchange
                    sendResponseBody(response, exchange);
                } else {

                }

            } else if (method.equals("POST")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);
                // Map the JSON to an object
                NewFeedback newfeedback = obj.readValue(reqBody, NewFeedback.class);

                // Create SQL Query with Object
                String sql = "INSERT INTO Feedback(Feedback_rating, Feedback_comments, Lesson_id, Course_id) VALUES ('"
                        + newfeedback.getRating() + "','" + newfeedback.getComments() + "','"
                        + newfeedback.getLessonid() + "','" + newfeedback.getCourseid() + "');";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send the response body and close exchange
                sendResponseBody(response, exchange);

            } else if (method.equals("OPTIONS")) {
                exchange.getResponseHeaders().add("Allowed", "GET, OPTIONS, POST, DELETE");
                exchange.sendResponseHeaders(200, exchange.getResponseHeaders().size());

            } else if (method.equals("DELETE")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);

                String sql = "DELETE FROM Feedback WHERE Feedback_id = " + reqBody + ";";

                System.out.println(sql);

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }
        }
    };

    @Override
    public HttpHandler getHandler() {
        return feedbackHandler;
    }

}
