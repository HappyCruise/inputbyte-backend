package Handlers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

import Objects.Course;
import Objects.NewCourse;
import Objects.EditCourse;

public class CourseHandler extends Handler implements IHandler {

    /**
     * Create the handler with given Connection
     * @param _connection   the Connection
     */
    public CourseHandler(Connection _connection) {
        super(_connection);
    }

    // Handler for /api/courses
    private HttpHandler courseHandler = new HttpHandler() {

        /**
         * Handle calls to course
         */
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            // Get the request method
            String method = exchange.getRequestMethod();

            // Add headers to response.
            addResponseHeaders(exchange);

            // Add the opening '[' to response for mapping multiple objects to JSON
            String response = "[";
            
            // Create the JSON mapper
            ObjectMapper obj = new ObjectMapper();

            // Compare request type
            if (method.equals("GET")) {

                // Return all Courses
                String sql = "SELECT * FROM Course";

                String reqURI = exchange.getRequestURI().getQuery();

                if(reqURI != null){
                    //Map params
                    Map <String,String> parms = queryToMap(reqURI);

                    //define ids
                    String userid= parms.get("userid");
                    String id = parms.get("id");
    
                    //Check if user or course id defined
                    if(userid != null  && !userid.equals("")){
                        sql += " WHERE User_id = " + userid;
                    }else if(id != null && !id.equals("")){
                        sql += " WHERE Course_id = " + id;
                    }
                }

               sql += ";";


                try (PreparedStatement statement = connection.prepareStatement(sql)) {

                    // Map the result to a resultset
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {

                        // Map results to a Course object
                        Course course = new Course(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3));

                        // Map the course object to JSON and add to response
                        try {
                            response += obj.writeValueAsString(course);
                            response += ",";

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                catch (SQLException e) {
                    e.printStackTrace();
                }

                // Remove ',' after last object and add enclosing ']'
                response = response.substring(0, response.length() - 1) + "]";
                // Send response headers
                exchange.sendResponseHeaders(200, response.getBytes().length);

                // Send response body and close exchange
                sendResponseBody(response, exchange);

            } else if (method.equals("POST")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);
                // Map the JSON to an object
                NewCourse newcourse = obj.readValue(reqBody, NewCourse.class);

                // Create SQL Query with Object
                String sql = "INSERT INTO Course(Course_name, User_id) VALUES ('" + newcourse.getName() + "','"
                        + newcourse.getTeacher_id() + "');";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send the response body and close exchange
                sendResponseBody(response, exchange);

            }
            // If a browser sends an options request
            else if (method.equals("OPTIONS")) {
                exchange.getResponseHeaders().add("Allowed", "GET, OPTIONS, POST, DELETE, PUT");
                exchange.sendResponseHeaders(200, exchange.getResponseHeaders().size());

            }
             
            else if (method.equals("PUT")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);
                // Map the string to an object
                EditCourse editcourse = obj.readValue(reqBody, EditCourse.class);

                
                String sql =  "Update Course SET  Course_name = '"+editcourse.getName() + 
                "', User_id = " +editcourse.getUser_id() + 
                " WHERE Course_id = " +editcourse.getId()+
                ";";

                System.out.println(sql);
                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }

            else if (method.equals("DELETE")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);

                String sql = "DELETE FROM Course WHERE Course_id = " + reqBody + ";";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }
        }

    };

    public HttpHandler getHandler(){
        return courseHandler;
    }

}
