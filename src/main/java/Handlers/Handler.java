package Handlers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

public abstract class Handler {
    /**
     * The database connection
     */
    Connection connection;

    /**
     * Create the handler with given Connection
     * @param _connection   the Connection
     */
    public Handler(Connection _connection){
        this.connection = _connection;
    }

    /**
     * Add response headers to given exchange
     * 
     * @param exchange the HttpExchange
     */        
    protected void addResponseHeaders(HttpExchange exchange) {
        exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
        exchange.getResponseHeaders().add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        exchange.getResponseHeaders().add("Access-Control-Allow-Headers", "Content-Type");
        exchange.getResponseHeaders().add("Content-Type", "application/json");
    }
    
    /**
     * Send response body to given HttpExchange with given string body
     * 
     * @param response      the body
     * @param exchange      the HttpExchange
     */
    protected void sendResponseBody(String response, HttpExchange exchange) {
        OutputStream output = exchange.getResponseBody();
        try {
            output.write(response.getBytes());
            output.flush();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Close the exchange
        exchange.close();
    }
    
    /**
     * Edit values in the database in the given Exchange with given PreparedStatement
     * 
     * @param statement
     * @param exchange
     * @return              response body depending on success status of the statement
     * @throws IOException
     */
    protected String editDatabaseValues(PreparedStatement statement, HttpExchange exchange) throws IOException {
        String returnStr = "";
        try {
            statement.executeQuery();
            // If successful response
            returnStr = "{\"success\":\"true\",\"status\":200}";
            exchange.sendResponseHeaders(200, returnStr.getBytes().length);

        } catch (SQLException e) {
            // If sql fails, most likely form value error
            returnStr = "{\"success\":\"false\", \"status\":400}";
            exchange.sendResponseHeaders(400, returnStr.getBytes().length);

        }
        return returnStr;
    }

    /**
     * Converts the given InputStream to JSON string
     * 
     * @param input         InputStream with JSON data
     * @return              JSON String
     * @throws IOException
     */
    protected String requestBodyToJSONStr(InputStream input) throws IOException {

        String line;
        // Create new BufferedReader to read input
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));

        // Map the result to a string
        StringBuilder responseBuilder = new StringBuilder();

        // Go through the request body
        while ((line = reader.readLine()) != null) {
            responseBuilder.append(line);
        }

        reader.close();

        // Return the String
        return responseBuilder.toString();

    }
    
    /**
     * Turns the query string to a map of parameters and their values
     * 
     * @param query     query string
     * @return          String map of parameters and values
     */
    protected static Map<String, String> queryToMap(String query){
        Map<String, String> result = new HashMap<String, String>();
    
        for (String param : query.split("&")) {
            String pair[] = param.split("=");

            if (pair.length>1) {
                result.put(pair[0], pair[1]);
    
            }else{
                result.put(pair[0], "");
            }
        }
        return result;
      }
    
}
