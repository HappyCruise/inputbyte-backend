package Handlers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import Objects.Lesson;
import Objects.NewLesson;
import Objects.EditLesson;

public class LessonHandler extends Handler implements IHandler {
    /**
     * Create the handler with given Connection
     * @param _connection   the Connection
     */
    public LessonHandler(Connection _connection) {
        super(_connection);
    }

    // Handler for /api/lessons
    public HttpHandler lessonHandler = new HttpHandler() {
        /**
         * Handle lesson calls
         * 
         * @param exchange the HttpExchange
         */
        @Override
        public void handle(HttpExchange exchange) throws IOException {

            // Get the request method
            String method = exchange.getRequestMethod();

            // Add headers to response.
            addResponseHeaders(exchange);

            // Add the opening '[' to response for mapping to JSON
            String response = "[";

            // Create the JSON mapper
            ObjectMapper obj = new ObjectMapper();

            // Compare request type
            if (method.equals("GET")) {

                String sql = "SELECT * FROM Lesson";
                String reqURI = exchange.getRequestURI().getQuery();

                if (reqURI != null) {
                    // Map params
                    Map<String, String> parms = queryToMap(reqURI);

                    // define ids
                    String userid = parms.get("userid");
                    String id = parms.get("id");
                    String code = parms.get("code");
                    String courseid = parms.get("courseid");

                    // Check if user or course id defined
                    if (userid != null && !userid.equals("")) {
                        sql += " WHERE Course_id IN (SELECT Course_id FROM Course WHERE User_id = " + userid + ")";
                    } else if (id != null && !id.equals("")) {
                        sql += " WHERE Lesson_id = " + id;
                    } else if (code != null && !code.equals("")) {
                        sql += " WHERE Lesson_pswd = '" + code + "'";
                    } else if (courseid != null && !courseid.equals("")) {
                        sql += " WHERE Course_id = " + courseid;
                    }
                }
                sql += ";";

                // Call the database
                try (PreparedStatement statement = connection.prepareStatement(sql)) {

                    // Map the result to a resultset
                    ResultSet resultSet = statement.executeQuery();
                    while (resultSet.next()) {

                        // Map results to a Lesson object
                        Lesson lesson = new Lesson(resultSet.getInt(1), resultSet.getDate(2), resultSet.getString(3),
                                resultSet.getString(4), resultSet.getString(5), resultSet.getInt(6));

                        try {
                            // Map the course object to JSON and add to response
                            response += obj.writeValueAsString(lesson);
                            response += ",";

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                // Remove ',' after last object and add enclosing ']'
                response = response.substring(0, response.length() - 1) + "]";

                // Send response headers
                exchange.sendResponseHeaders(200, response.getBytes().length);

                // Send response body and close exchange
                sendResponseBody(response, exchange);

            } else if (method.equals("POST")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);
                
                // Map the string to an object
                NewLesson newlesson = obj.readValue(reqBody, NewLesson.class);


                // Create SQL Query with object
                String sql = "INSERT INTO Lesson(Lesson_time, Lesson_room, Lesson_pswd, Lesson_description, Course_id) VALUES ('"
                        + newlesson.getTime() + "','" + newlesson.getRoom() + "','" + newlesson.getLessonpassword()
                        + "','" + newlesson.getDescription() + "','" + newlesson.getCourseid() + "');";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send the response body and close exchange
                sendResponseBody(response, exchange);

            } 
            
            else if (method.equals("OPTIONS")) {
                exchange.getResponseHeaders().add("Allowed", "GET, OPTIONS, POST, DELETE, PUT");
                exchange.sendResponseHeaders(201, 0);

            } 
            else if (method.equals("PUT")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);
                // Map the string to an object
                EditLesson editlesson = obj.readValue(reqBody, EditLesson.class);

                
                String sql =  "Update Lesson SET  Lesson_time = '"+editlesson.getDate() + 
                "', Lesson_room = '" + editlesson.getRoom() + 
                "', Lesson_pswd = '" + editlesson.getLessonpassword() + 
                "', Lesson_description = '" + editlesson.getDescription() + 
                "', Course_id = '" + editlesson.getCourseId() + 
                "' WHERE Lesson_id = " + editlesson.getId() +
                ";";

                System.out.println(sql);
                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }

            else if (method.equals("DELETE")) {

                // Get request body
                InputStream input = exchange.getRequestBody();
                // Map body to JSON String
                String reqBody = requestBodyToJSONStr(input);

                String sql = "DELETE FROM Lesson WHERE Lesson_id = " + reqBody + ";";

                // Call the database
                try {
                    response = editDatabaseValues(connection.prepareStatement(sql), exchange);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Send response body and close exchange
                sendResponseBody(response, exchange);
            }
        }

    };
    /**
     * Return the handler
     * 
     * @return lessonHandler
     */
    @Override
    public HttpHandler getHandler() {
        return lessonHandler;
    }

}
